package com.example;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Sort {
    Users user1 = new Users(Firstname.Alexey, Secondname.Zhir, PhoneNumber.n0507676884);
    Users user2 = new Users(Firstname.Ilya, Secondname.Dubinskiy, PhoneNumber.n0636530064);
    Users user3 = new Users(Firstname.Evgeniy, Secondname.Shapovalov, PhoneNumber.n0931299466);
    Users user4 = new Users(Firstname.Roman, Secondname.Zverev, PhoneNumber.n0994356722);
    Users user5 = new Users(Firstname.Dmitriy, Secondname.Pinchuk, PhoneNumber.n0663123232);
    Users user6 = new Users(Firstname.Ilya, Secondname.Gayvoronsky, PhoneNumber.n0664356888);
    Users user7 = new Users(Firstname.Danil, Secondname.Voloshchenko, PhoneNumber.n0677455679);
    Users user8 = new Users(Firstname.Oleg, Secondname.Diryavko, PhoneNumber.n0985432285);

    List<Users> adressUsers = new ArrayList<>(8);

    public void setUsers(){
        adressUsers.add(0, user1);
        adressUsers.add(1, user2);
        adressUsers.add(2, user3);
        adressUsers.add(3, user4);
        adressUsers.add(4, user5);
        adressUsers.add(5, user6);
        adressUsers.add(6, user7);
        adressUsers.add(7, user8);
    }

    public void SortByFirstname() {
        List<String> sortByFirstname = adressUsers.stream()
                .sorted((o1, o2) -> o1.getFirstName().toString().compareTo(o2.getFirstName().toString()))
                .map(user -> "First Name: " + user.getFirstName() + "| Second Name: " + user.getSecondName() + "| Phone Number: " + user.getPhoneNumber())
                .collect(Collectors.toList());
        sortByFirstname.forEach(sort->System.out.println(sort));
    }

    public void SortBySecondname() {
        List<String> sortBySecondName = adressUsers.stream()
                .sorted((o1, o2) -> o1.getSecondName().toString().compareTo(o2.getSecondName().toString()))
                .map(user -> "First Name: " + user.getFirstName() + "| Second Name: " + user.getSecondName() + "| Phone Number: " + user.getPhoneNumber())
                .collect(Collectors.toList());
        sortBySecondName.forEach(sort->System.out.println(sort));
    }

    public void SortBySecondnameAndFirstname() {
        List<String> sortBySecondNameAndFirstName = adressUsers.stream()
                .sorted((o1, o2) -> o1.getFirstName().toString().compareTo(o2.getFirstName().toString()) & o1.getSecondName().toString().compareTo(o2.getSecondName().toString()))
                .map(user -> "First Name: " + user.getFirstName() + "| Second Name: " + user.getSecondName() + "| Phone Number: " + user.getPhoneNumber())
                .collect(Collectors.toList());
        sortBySecondNameAndFirstName.forEach(sort->System.out.println(sort));
    }

    public void SortByPhoneNumber() {
        List<String> sortByPhoneNumber = adressUsers.stream()
                .sorted((o1, o2) -> o1.getPhoneNumber().toString().compareTo(o2.getPhoneNumber().toString()))
                .map(user -> "First Name: " + user.getFirstName() + "| Second Name: " + user.getSecondName() + "| Phone Number: " + user.getPhoneNumber())
                .collect(Collectors.toList());
        sortByPhoneNumber.forEach(sort->System.out.println(sort));
    }
}
