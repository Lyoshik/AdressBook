package com.example;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Sort sort = new Sort();
        sort.setUsers();
        System.out.println("Hello! This is a simple Phone Book.");
        System.out.println("You can sort users by first name, second name and phone number.");
        System.out.println("Please, choose type of sort! (Press '1' for sorting by first name | '2' - second name | '3' - phone number):");

        String x = scanner.next();
        if (x.equals("1")){
            sort.SortByFirstname();
        }
        else if (x.equals("2")){
            sort.SortBySecondname();
        }
        else if (x.equals("3")){
            sort.SortBySecondnameAndFirstname();
        }
        else if (x.equals("4")){
            sort.SortByPhoneNumber();
        }
        else {
            System.out.println("Wrong command!");
        }
    }
}
