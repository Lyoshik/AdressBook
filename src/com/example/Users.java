package com.example;

public class Users {
    Firstname firstname;
    Secondname secondname;
    PhoneNumber phoneNumber;

    public Users(Firstname firstname, Secondname secondname, PhoneNumber phoneNumber){
        this.firstname = firstname;
        this.secondname = secondname;
        this.phoneNumber = phoneNumber;
    }

    public Firstname getFirstName(){
        return firstname;
    }

    public Secondname getSecondName(){
        return secondname;
    }

    public PhoneNumber getPhoneNumber(){
        return phoneNumber;
    }
}
